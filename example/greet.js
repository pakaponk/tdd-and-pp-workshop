function greet(name) {
  if (name) {
    return `Hello ${name}`;
  }
  return 'Hello World';
}

export default greet;
