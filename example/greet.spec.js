import greet from './greet';

describe('greet test suites', () => {
  test('Should return "Hello World" when name is not given', () => {
    expect(greet()).toBe('Hello World');
  });
  test('Should return "Hello Earth" when "Earth" is given as a name', () => {
    expect(greet('Earth')).toBe('Hello Earth');
  });
});
